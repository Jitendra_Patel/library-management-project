package com.example.UserService.controller;

import com.example.UserService.dto.*;
import com.example.UserService.entity.User;
import com.example.UserService.mapper.UserMapper;
import com.example.UserService.repository.CartItemRepository;
import com.example.UserService.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Controller
public class UserController {
    @Autowired
    private UserService userService;


    @GetMapping("/login")
    public String login() {
        return "loginPage";
    }

    @GetMapping("/signup")
    public String signupPageAdmin(Model model) {
        try {
            model.addAttribute("user", new UserRequest());
            return "signupPage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @PostMapping("/saveUser")
    public String saveAdmin(@Valid @ModelAttribute("user") UserRequest userRequest,
                            BindingResult result, Model model) {
        try {
            if (result.hasErrors()) {
                model.addAttribute("user", userRequest);
                return "signupPage";
            }
            userService.saveUser(userRequest);
            return "redirect:/login";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/rent-book/{bookId}")
    public String bookRent(@PathVariable Long bookId, Model model) {
        try {
            userService.findBookById(bookId);
            model.addAttribute("book", bookId);
            model.addAttribute("bookTransaction", new BookRentAndReturnRequest());
            return "rentForm";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @PostMapping("/rent")
    public String saveRentBook(@RequestParam Long bookId, Model model,
                       @ModelAttribute("bookTransaction") @Valid BookRentAndReturnRequest bookRentAndReturnRequest,
                       BindingResult result){
        try {
            if (result.hasErrors()) {
                model.addAttribute("bookTransaction", bookRentAndReturnRequest);
                return "rentForm";
            }
            User loginUser = getLoginUser();
            BookRentAndReturnTransactionResponse rentResponse = userService.rentBook(bookId, bookRentAndReturnRequest, loginUser.getId());
            model.addAttribute("bookTransaction", rentResponse);
            model.addAttribute("bookId", bookId);
            model.addAttribute("response", rentResponse.getMessage());
            return "transactionMessage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }

    }
    @GetMapping("/return-book/{bookId}")
    public String returnBook(@PathVariable Long bookId,Model model){
        try {
            userService.findBookById(bookId);
            model.addAttribute("book", bookId);
            model.addAttribute("bookTransaction", new BookRentAndReturnRequest());
            return "returnForm";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @PostMapping("/return")
    public String saveReturnBook(@RequestParam Long bookId, Model model,
                       @ModelAttribute("bookTransaction") BookRentAndReturnRequest bookRentAndReturnRequest){
        try {
            User loginUser = getLoginUser();
            BookRentAndReturnTransactionResponse returnResponse = userService.returnBook(bookId, loginUser.getId(), bookRentAndReturnRequest);
            model.addAttribute("bookTransaction", returnResponse);
            model.addAttribute("bookId", bookId);
            model.addAttribute("response", returnResponse.getMessage());
            return "transactionMessage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }
    @GetMapping("/purchase-book/{bookId}")
    public String purchaseBook(@PathVariable Long bookId,Model model){
        try {
            userService.findBookById(bookId);
            model.addAttribute("book", bookId);
            model.addAttribute("bookTransaction", new BookRentAndReturnRequest());
            return "purchaseForm";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @PostMapping("/purchase-book")
    public String purchaseBook(@RequestParam Long bookId,BookRentAndReturnRequest bookRentAndReturnRequest,
                               Model model){
        try {
            userService.findBookById(bookId);
            User loginUser = getLoginUser();
            bookRentAndReturnRequest.setPurchaseDate(LocalDate.now().toString());
            BookPurchaseResponse bookPurchaseResponse = userService.purchaseBook(bookId, loginUser.getId(), bookRentAndReturnRequest);
            String delete = "delete";
            model.addAttribute("bookId", bookId);
            model.addAttribute("purchaseResponse", bookPurchaseResponse.getMessage());
            if (bookPurchaseResponse.getMessage().equals("This Book has already deleted select other book") ||
                    bookPurchaseResponse.getMessage().equals("This Book is not available in stock sorry for inconvenience")) {
                model.addAttribute("delete", delete);
            }
            model.addAttribute("bookTransaction", bookPurchaseResponse);
            return "transactionMessage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }

    }


    @PostMapping("/premium-member")
    public String premiumMembership(Model model){
        try {
            User loginUser = getLoginUser();
            UserRequest userRequest = UserMapper.entityToDto(loginUser);
            userService.savePremiumUser(userRequest);
            model.addAttribute("user", loginUser);
            return "redirect:/homepage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/purchase-list")
    public String getAllPurchaseListOfUser(Model model){
        try {
            User loginUser = getLoginUser();
            List<BookPurchaseResponse> purchaseListOfBook = userService.getPurchseListOfBook(loginUser.getId());
            model.addAttribute("name", loginUser.getName());
            model.addAttribute("purchaseResponse", purchaseListOfBook);
            return "profilePage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/rent-return-list")
    public String getAllRentAndReturnListOfUser(Model model){
        try {
            User loginUser = getLoginUser();
            List<BookRentAndReturnTransactionResponse> rentAndReturnTransactionListOfBooks =
                    userService.getRentAndReturnTransactionListOfBooks(loginUser.getId());
            model.addAttribute("name", loginUser.getName());
            model.addAttribute("rentResponse", rentAndReturnTransactionListOfBooks);
            return "profilePage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/trending-purchase-books")
    public String listOfHighestPurchasedBook(Model model){
        try {
            List<TrendingBookResponse> purchases = userService.countBookPurchaseByAllUser();
            model.addAttribute("objects", purchases);
            return "trendingPage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/trending-rented-books")
    public String listOfHighestRentedBook(Model model){
        try {
            List<TrendingBookResponse> countBookRentedByAllUsers = userService.countBookRentedByAllUsers();
            model.addAttribute("object", countBookRentedByAllUsers);
            return "trendingPage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/homepage")
    public String paginationOnBookList(Model model,
                                       @RequestParam(defaultValue = "0",required = false) Integer page,
                                       @RequestParam(defaultValue = "5",required = false) Integer pageSize){
        try {
            List<BookResponse> bookResponses = userService.paginationOnBook(page, pageSize);
            BookResponse response = bookResponses.get(0);
            Integer totalPages = response.getTotalPages();
            User loginUser = getLoginUser();
            Optional<User> user = userService.findByEmail(loginUser.getUsername());
            if (user.get().getEliteUser() == null) {
                String value = "not elite";
                model.addAttribute("elite", value);
            }
            model.addAttribute("books", bookResponses);
            model.addAttribute("currentPage", page);
            model.addAttribute("totalPages", totalPages);
            return "homePage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }


    @GetMapping("/profile")
    public String getAllTransactionOfUser(Model model){
        try {
            User loginUser = getLoginUser();
            List<BookTransactionResponse> allTransaction = userService.getAllTransactionOfUserByUserId(loginUser.getId());
            model.addAttribute("transaction", allTransaction);
            model.addAttribute("username", loginUser.getUsername());
            model.addAttribute("name", loginUser.getName());
            String table = "table";
            model.addAttribute("table", table);
            return "profilePage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/add-to-cart/{bookId}")
    public String addToCart(Model model,@PathVariable Long bookId){
        try {
            userService.findBookById(bookId);
            model.addAttribute("book", bookId);
            model.addAttribute("cartItem", new CartItemRequest());
            return "addToCartForm";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @PostMapping("/save-cart-item")
    public String saveCart(@RequestParam Long bookId,@ModelAttribute("cartItem") CartItemRequest addToCart,
                           Model model){
        try {
            User loginUser = getLoginUser();
            CartItemResponse cartItemResponse = userService.saveCartItems(bookId, addToCart, loginUser.getId());
            BookResponse bookById = userService.getBookById(bookId);
            Double discountedPrice = userService.getDiscountedPrice(cartItemResponse, bookById);
            model.addAttribute("cartItem", bookById);
            model.addAttribute("quantity", addToCart.getQuantity());
            model.addAttribute("discountedPrice", discountedPrice);
            model.addAttribute("status", "Add to cart done successfully");
            return "cartTransactionMessage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/view-cart")
    public String viewCart(Model model){
        try {
            User loginUser = getLoginUser();
            List<CartItemResponse> responses = userService.viewCartDetailOfUser(loginUser.getId());
            model.addAttribute("responses", responses);
            Integer size = responses.size();
            if (size > 0) {
                model.addAttribute("total", responses.get(size - 1).getTotalBookPrice());
            }
            return "viewCart";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/update-cart/{bookId}/{elite}")
    public String updateCart(@PathVariable String elite, @PathVariable Long bookId,Model model){
        try {
            userService.findBookById(bookId);
            model.addAttribute("book", bookId);
            model.addAttribute("cartItem", new CartItemRequest());
            model.addAttribute("elite", elite);
            System.out.println(elite);
            return "updateCart";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }


    @PostMapping("/update-cart-item")
    public String saveUpdatedCart(@ModelAttribute("cartItem") CartItemRequest cartItemRequest,
                                 @RequestParam String elite,@RequestParam Long bookId,Model model){
        try {
            User loginUser = getLoginUser();
            userService.updateCartDetail(loginUser.getId(), elite, bookId, cartItemRequest);
            return "redirect:/view-cart";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/delete-cart-item/{bookId}/{elite}")
    public String deleteCartItem(@PathVariable String elite,@PathVariable Long bookId,Model model){
        try {
            User loginUser = getLoginUser();
            userService.DeleteCartDetail(loginUser.getId(), elite, bookId);
            return "redirect:/view-cart";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/buy-cart-item/{bookId}/{elite}/{quantity}")
    public String buyItemOfCart(@PathVariable String elite, @PathVariable Long bookId,
                                @PathVariable String quantity,Model model){
        try {
            userService.findBookById(bookId);
            User loginUser = getLoginUser();
            BookPurchaseResponse bookPurchaseResponse = userService.buyBookFromCart(elite, bookId, quantity, loginUser.getId());
            String delete = "delete";
            model.addAttribute("bookId", bookId);
            model.addAttribute("purchaseResponse", bookPurchaseResponse.getMessage());
            if (bookPurchaseResponse.getMessage().equals("This Book has already deleted select other book")) {
                model.addAttribute("delete", delete);
            }
            model.addAttribute("bookTransaction", bookPurchaseResponse);
            return "transactionMessage";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/buy-all-books")
    public String buyAllBookOfCart(Model model){
        try {
            User loginUser = getLoginUser();
            List<BookPurchaseResponse> bookPurchaseResponse = userService.buyAllBooks(loginUser.getId());
            model.addAttribute("listOfResponse", bookPurchaseResponse);
            return "cartBookPurchase";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }

    }

    public User getLoginUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        Optional<User> user = userService.findByEmail(userDetails.getUsername());
        return user.get();
    }
}
