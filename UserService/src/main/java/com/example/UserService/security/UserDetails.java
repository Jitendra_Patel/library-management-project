package com.example.UserService.security;

import com.example.UserService.entity.User;
import com.example.UserService.exception.BookNotFoundException;
import com.example.UserService.exception.UserNotFoundException;
import com.example.UserService.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetails implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Override
    public org.springframework.security.core.userdetails.UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<User> user = userRepository.findByEmail(username);

        if(user == null){
            throw new UserNotFoundException("Admin is not present");
        }
        return new org.springframework.security.core.userdetails.User(user.get().getUsername(),user.get().getPassword(),user.get().getAuthorities());
    }
}
