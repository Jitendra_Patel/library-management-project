package com.example.UserService.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TrendingBookResponse {


    private Long bookId;
    private Long totalCount;
}
