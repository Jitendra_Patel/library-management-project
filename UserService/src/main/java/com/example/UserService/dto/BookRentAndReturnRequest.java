package com.example.UserService.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookRentAndReturnRequest {

    private String purchaseDate;

    private String expiryDate;

    private String returnDate;

    private Long userId;

    private Integer quantity;

    private Long bookId;

}
