package com.example.UserService.dto;

import lombok.*;

import javax.persistence.criteria.CriteriaBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CartItemResponse {

    private Long bookId;
    private Integer quantity;
    private String bookName;
    private Double originalPrice;
    private Double discountedPrice;
    private Double pricePerBook;
    private Double subTotal;
    private Boolean isElite;
    private Double totalBookPrice;
}
