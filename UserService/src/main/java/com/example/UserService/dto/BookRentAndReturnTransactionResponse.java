package com.example.UserService.dto;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookRentAndReturnTransactionResponse {

    private LocalDate purchaseDate;

    private LocalDate expectedReturnDate;

    private LocalDate returnDate;

    private Double paidRent;

    private Double bookPenalty;

    private Double totalRent;

    private Long userId;

    private Long  bookId;

    private String bookName;

    private Double originalBookRent;

    private Double discountedBookRent;

    private String message;

}
