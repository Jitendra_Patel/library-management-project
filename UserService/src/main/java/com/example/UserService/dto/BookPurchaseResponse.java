package com.example.UserService.dto;

import lombok.*;

import java.time.LocalDate;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Builder
public class BookPurchaseResponse {


    private Double originalPrice;

    private Double discountedPrice;

    private Integer quantity;

    private Double pricePerBook;

    private Double totalAmount;

    private Long userId;

    private LocalDate purchaseDate;

    private Long bookId;

    private String message;

    private String bookName;

}