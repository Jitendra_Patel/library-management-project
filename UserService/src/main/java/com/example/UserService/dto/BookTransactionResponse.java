package com.example.UserService.dto;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookTransactionResponse {

    private Integer openQuantity;

    private Integer updatedQuantity;

    private Double totalAmount;

    private LocalDate transactionDate;

    private String operation;

    private Integer quantity;

    private Long bookId;

    private String bookName;

    private Long userId;
}
