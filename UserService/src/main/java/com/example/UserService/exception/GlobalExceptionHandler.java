package com.example.UserService.exception;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(UserNotFoundException.class)
    public String handleUserNotFoundException(UserNotFoundException ex, Model model){
        model.addAttribute("errorMessage", ex.getMessage());
        return "errorPage";

    }

    @ExceptionHandler(BookNotFoundException.class)
    public String handleBookNotFoundException(BookNotFoundException ex ,Model model){
        model.addAttribute("errorMessage", ex.getMessage());
        return "errorPage";
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public String handleResourceNotFoundException(ResourceNotFoundException ex , Model model){
        model.addAttribute("errorMessage", ex.getMessage());
        return "errorPage";
    }
}
