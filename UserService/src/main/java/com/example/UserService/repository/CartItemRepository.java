package com.example.UserService.repository;

import com.example.UserService.entity.Cart;
import com.example.UserService.entity.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface CartItemRepository extends JpaRepository<CartItem,Long> {

    @Query(" SELECT c FROM CartItem c WHERE c.bookId = :bookId AND c.cart = :cart  AND " +
            " c.isElite = :isElite ")
    Optional<CartItem> findByBookIdAndCartId(@Param("bookId") Long bookId,@Param("cart") Cart cart,
                                             @Param("isElite") Boolean isElite);

    @Modifying
    @Query(value = "DELETE FROM cart_item WHERE cart_id = :cartId", nativeQuery = true)
    void deleteByCartId(@Param("cartId") Long cartId);






}
