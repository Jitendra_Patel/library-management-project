package com.example.UserService.repository;

import com.example.UserService.entity.EliteUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EliteUserRepository extends JpaRepository<EliteUser,Long> {
}
