package com.example.UserService.mapper;

import com.example.UserService.dto.CartItemRequest;
import com.example.UserService.dto.CartItemResponse;
import com.example.UserService.entity.CartItem;

public class CartItemMapper {

    public static CartItemResponse entityToDto(CartItem cartItem){
        return CartItemResponse.builder()
                .bookId(cartItem.getBookId())
                .quantity(cartItem.getQuantity())
                .build();
    }


}
