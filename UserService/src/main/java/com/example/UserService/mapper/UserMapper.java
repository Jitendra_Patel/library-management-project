package com.example.UserService.mapper;

import com.example.UserService.dto.UserRequest;
import com.example.UserService.entity.User;

public class UserMapper {

    public static User dtoToEntity(UserRequest userRequest){

        return User.builder()
                .email(userRequest.getEmail())
                .name(userRequest.getName())
                .password(userRequest.getPassword())
                .build();
    }

    public static UserRequest entityToDto(User user){
        return UserRequest.builder()
                .id(user.getId())
                .email(user.getEmail())
                .name(user.getName())
                .password(user.getPassword())
                .role(user.getRole())
                .build();
    }


}
