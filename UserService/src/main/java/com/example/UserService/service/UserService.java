package com.example.UserService.service;

import com.example.UserService.dto.*;
import com.example.UserService.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

     void saveUser(UserRequest userRequest);

    List<BookResponse> getAllBooks();

    BookRentAndReturnTransactionResponse  rentBook(Long bookId, BookRentAndReturnRequest bookRentAndReturnRequest, Long userId);

    Optional<User> findByEmail(String username);

    BookRentAndReturnTransactionResponse returnBook(Long bookId, Long userId, BookRentAndReturnRequest bookRentAndReturnRequest);

    void savePremiumUser(UserRequest userRequest);

    BookPurchaseResponse purchaseBook(Long bookId, Long userId, BookRentAndReturnRequest bookRentAndReturnRequest);

    List<BookPurchaseResponse> getPurchseListOfBook(Long userId);

    List<BookRentAndReturnTransactionResponse> getRentAndReturnTransactionListOfBooks(Long userId);


    List<TrendingBookResponse> countBookPurchaseByAllUser();

    List<TrendingBookResponse> countBookRentedByAllUsers();

    List<BookResponse> paginationOnBook(Integer page, Integer pageSize);

    void findBookById(Long bookId);

    List<BookTransactionResponse> getAllTransactionOfUserByUserId(Long userId);

    CartItemResponse saveCartItems(Long bookId, CartItemRequest addToCart, Long userId);

    BookResponse getBookById(Long bookId);

    Double getDiscountedPrice(CartItemResponse response, BookResponse bookById);

    List<CartItemResponse> viewCartDetailOfUser(Long userId);

    void updateCartDetail(Long userId, String elite, Long bookId, CartItemRequest cartItemRequest);

    void DeleteCartDetail(Long userId, String elite, Long bookId);

    BookPurchaseResponse buyBookFromCart(String elite, Long bookId, String quantity,Long userId);

    List<BookPurchaseResponse> buyAllBooks(Long userId);



}
