package com.example.UserService.service.impl;

import com.example.UserService.dto.*;
import com.example.UserService.entity.Cart;
import com.example.UserService.entity.CartItem;
import com.example.UserService.entity.EliteUser;
import com.example.UserService.enums.Role;
import com.example.UserService.entity.User;
import com.example.UserService.exception.BookNotFoundException;
import com.example.UserService.exception.ResourceNotFoundException;
import com.example.UserService.exception.UserNotFoundException;
import com.example.UserService.mapper.UserMapper;
import com.example.UserService.repository.CartItemRepository;
import com.example.UserService.repository.CartRepository;
import com.example.UserService.repository.EliteUserRepository;
import com.example.UserService.repository.UserRepository;
import com.example.UserService.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Value("${auth.user-name}")
    private String authUserName;
    @Value("${auth.password}")
    private String authSecret;
    @Value("${admin.base.url}")
    private String adminBaseUrl;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private EliteUserRepository eliteUserRepository;

    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private CartItemRepository cartItemRepository;


    public void saveUser(UserRequest userRequest) {
        User user = UserMapper.dtoToEntity(userRequest);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(Role.ROLE_USER);
        Cart cart = new Cart();
        cart.setUser(user);
        user.setCart(cart);
        userRepository.save(user);

    }
    @Override
    public List<BookResponse> getAllBooks() {
        String apiUrl = adminBaseUrl.concat("/list-of-books");
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName, authSecret);

        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<List<BookResponse>> response = restTemplate.exchange(
                apiUrl,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<BookResponse>>() {
                }
        );
        return  response.getBody();
    }

    @Override
    public BookRentAndReturnTransactionResponse rentBook(Long bookId,BookRentAndReturnRequest bookRentAndReturnRequest,Long userId) {
        String apiUrl = adminBaseUrl.concat("/rent-book/" +bookId+"/"+userId);
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName, authSecret);
        headers.setContentType(MediaType.APPLICATION_JSON);
        Optional<User> user = userRepository.findById(userId);
        if(user.get().getEliteUser() !=null ){
            bookRentAndReturnRequest.setUserId(user.get().getEliteUser().getUser().getId());
        }
        else{
            bookRentAndReturnRequest.setUserId(-1l);
        }
        HttpEntity<BookRentAndReturnRequest> entity = new HttpEntity<>(bookRentAndReturnRequest, headers);
        ResponseEntity<BookRentAndReturnTransactionResponse> response = restTemplate.exchange(
                apiUrl,
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<BookRentAndReturnTransactionResponse>() {
                }
        );
        return response.getBody();
    }

    @Override
    public Optional<User> findByEmail(String username) {
        Optional<User> user = userRepository.findByEmail(username);
        if(user.isEmpty()){
            throw new UserNotFoundException("User is not present");
        }
        return user;
    }

    @Override
    public BookRentAndReturnTransactionResponse returnBook(Long bookId, Long userId,
                           BookRentAndReturnRequest bookRentAndReturnRequest) {
        String apiUrl = adminBaseUrl.concat("/return-book/" +bookId+"/"+userId);

        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName, authSecret);
        headers.setContentType(MediaType.APPLICATION_JSON);
        Optional<User> user = userRepository.findById(userId);
        if(user.get().getEliteUser() !=null ){
            bookRentAndReturnRequest.setUserId(user.get().getEliteUser().getUser().getId());
        }
        else{
            bookRentAndReturnRequest.setUserId(-1l);
        }
        HttpEntity<BookRentAndReturnRequest> entity = new HttpEntity<>(bookRentAndReturnRequest, headers);

        ResponseEntity<BookRentAndReturnTransactionResponse> response = restTemplate.exchange(
                apiUrl,
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<BookRentAndReturnTransactionResponse>() {
                }
        );
        System.out.println(response.getBody());
        return response.getBody();

    }

    @Override
    public void savePremiumUser(UserRequest userRequest) {
        EliteUser eliteUser = new EliteUser();
        User user = UserMapper.dtoToEntity(userRequest);
        user.setId(userRequest.getId());
        eliteUser.setUser(user);
        eliteUserRepository.save(eliteUser);
    }

    @Override
    public BookPurchaseResponse purchaseBook(Long bookId, Long userId, BookRentAndReturnRequest bookRentAndReturnRequest) {
        String apiUrl = adminBaseUrl.concat("/purchase-book/" +bookId+"/"+userId);
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName, authSecret);
        headers.setContentType(MediaType.APPLICATION_JSON);
        Optional<User> user = userRepository.findById(userId);
        if(user.get().getEliteUser() !=null ){
            bookRentAndReturnRequest.setUserId(user.get().getEliteUser().getUser().getId());
        }
        else{
            bookRentAndReturnRequest.setUserId(-1l);
        }
        HttpEntity<BookRentAndReturnRequest> entity = new HttpEntity<>(bookRentAndReturnRequest, headers);
        ResponseEntity<BookPurchaseResponse> response = restTemplate.exchange(
                apiUrl,
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<BookPurchaseResponse>() {
                }
        );

        return response.getBody();

    }


    @Override
    public List<BookPurchaseResponse> getPurchseListOfBook(Long userId) {
        String apiUrl = adminBaseUrl.concat("/user-purchase-list/"+ userId);

        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName, authSecret);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<List<BookPurchaseResponse>> response = restTemplate.exchange(
                apiUrl,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<BookPurchaseResponse>>() {
                }
        );
        return  response.getBody();
    }

    @Override
    public List<BookRentAndReturnTransactionResponse> getRentAndReturnTransactionListOfBooks(Long userId) {
        String apiUrl = adminBaseUrl.concat("/user-rent-return-list/"+ userId);
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName, authSecret);

        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<List<BookRentAndReturnTransactionResponse>> response = restTemplate.exchange(
                apiUrl,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<BookRentAndReturnTransactionResponse>>() {
                }
        );
        return  response.getBody();
    }

    @Override
    public List<TrendingBookResponse> countBookPurchaseByAllUser() {
        String apiUrl = adminBaseUrl.concat("/trending-purchase-book");
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName, authSecret);

        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<List<TrendingBookResponse>> response = restTemplate.exchange(
                apiUrl,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<TrendingBookResponse>>() {
                }
        );
        return  response.getBody();
    }

    @Override
    public List<TrendingBookResponse> countBookRentedByAllUsers() {
        String apiUrl = adminBaseUrl.concat("/trending-rented-book");
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName,authSecret);

        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<List<TrendingBookResponse>> response = restTemplate.exchange(
                apiUrl,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<TrendingBookResponse>>() {
                }
        );
        return  response.getBody();
    }

    @Override
    public List<BookResponse> paginationOnBook(Integer page, Integer pageSize) {
        String apiUrl = adminBaseUrl.concat("/pagination?page="+page);
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName, authSecret);

        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<List<BookResponse>> response = restTemplate.exchange(
                apiUrl,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<BookResponse>>() {
                }
        );
        return  response.getBody();
    }

    @Override
    public void findBookById(Long bookId) {
        String apiUrl = adminBaseUrl.concat("/get-book/"+bookId);

        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName, authSecret);

        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(
                apiUrl,
                HttpMethod.GET,
                entity,
                String.class
        );
        String responseMessage = response.getBody();
        if(responseMessage.length()>15){
            throw new BookNotFoundException(responseMessage);
    }

    }

    @Override
    public List<BookTransactionResponse> getAllTransactionOfUserByUserId(Long userId) {
        String apiUrl=adminBaseUrl.concat("/get-transaction/"+userId);
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName,authSecret);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<List<BookTransactionResponse>> responseEntity = restTemplate.exchange(
                apiUrl,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<BookTransactionResponse>>() {
                }
        );
        return responseEntity.getBody();
    }

    @Override
    public CartItemResponse saveCartItems(Long bookId, CartItemRequest addToCart, Long userId) {
        Optional<User> user = userRepository.findById(userId);
        if (user.isEmpty()) {
            throw new UserNotFoundException("User id not present");
        }
        Cart userCart = user.get().getCart();
        Boolean isEliteUser ;
        if(user.get().getEliteUser() != null){
            isEliteUser = true;
        }
        else{
            isEliteUser = false;
        }


        if(cartItemRepository.findByBookIdAndCartId(bookId,userCart,isEliteUser).isEmpty()) {
            CartItem cartItem = new CartItem();
            cartItem.setBookId(bookId);
            cartItem.setQuantity(addToCart.getQuantity());
            cartItem.setCart(user.get().getCart());
            cartItem.setIsElite(isEliteUser);

            Cart cart = user.get().getCart();
            cart.getCartItemList().add(cartItem);
            cartRepository.save(cart);
        }
        else{

            Optional<CartItem> cartItem = cartItemRepository.findByBookIdAndCartId(bookId, userCart, isEliteUser);
            cartItem.get().setQuantity(cartItem.get().getQuantity()+addToCart.getQuantity());
            cartItemRepository.save(cartItem.get());
        }


        CartItemResponse response = new CartItemResponse();
        response.setIsElite(isEliteUser);

        return response;
    }

    @Override
    public BookResponse getBookById(Long bookId) {
        String apiUrl = adminBaseUrl.concat("/book-by-id/"+bookId);
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName,authSecret);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<BookResponse> response = restTemplate.exchange(
                apiUrl,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<BookResponse>() {
                }
        );
        return response.getBody();

    }

    @Override
    public Double getDiscountedPrice(CartItemResponse response, BookResponse bookById) {

        double discountedPrice;
        if(response.getIsElite()){
            discountedPrice= (bookById.getPrice() * 80 / 100);
        }
        else {
            discountedPrice=(bookById.getPrice());
        }
        return discountedPrice;
    }

    @Override
    public List<CartItemResponse> viewCartDetailOfUser(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        if (user.isEmpty()) {
            throw new UserNotFoundException("User is not present in DB");
        }

        List<CartItem> cartItemList = user.get().getCart().getCartItemList();
        List<CartItemResponse> responses = new ArrayList<>();
        Double totalAmount= 0.0;
        for(CartItem cartItem : cartItemList){

            CartItemResponse cartItemResponse = new CartItemResponse();
            BookResponse bookById = getBookById(cartItem.getBookId());
            cartItemResponse.setBookId(bookById.getId());
            cartItemResponse.setBookName(bookById.getTitle());
            cartItemResponse.setQuantity(cartItem.getQuantity());
            cartItemResponse.setOriginalPrice(bookById.getPrice());
            cartItemResponse.setDiscountedPrice(bookById.getPrice()*80/100);
            cartItemResponse.setIsElite(cartItem.getIsElite());
            if(cartItem.getIsElite()){
                cartItemResponse.setPricePerBook(bookById.getPrice()*80/100);
                cartItemResponse.setSubTotal((bookById.getPrice()*80/100)*cartItem.getQuantity());
                totalAmount += (bookById.getPrice()*80/100)*cartItem.getQuantity();
            }
            else{
                cartItemResponse.setPricePerBook(bookById.getPrice());
                cartItemResponse.setSubTotal((bookById.getPrice())*cartItem.getQuantity());
                totalAmount += (bookById.getPrice())*cartItem.getQuantity();
            }
            System.out.println(totalAmount);
            cartItemResponse.setTotalBookPrice(totalAmount);
            responses.add(cartItemResponse);
        }


      return responses;
    }

    @Override
    public void updateCartDetail(Long userId, String elite, Long bookId, CartItemRequest cartItemRequest) {
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty()){
            throw new UserNotFoundException("User is not present in DB");
        }
        Boolean eliteUser;
        if(elite.equals("true")){
            eliteUser=true;
        }
        else {
            eliteUser=false;
        }
        Optional<CartItem> cartItem = cartItemRepository.findByBookIdAndCartId(bookId, user.get().getCart(), eliteUser);
        if(cartItem.isEmpty()){
            throw new ResourceNotFoundException("CartItem is not present for given id");
        }
        else{
            cartItem.get().setQuantity(cartItemRequest.getQuantity());
        }
        cartItemRepository.save(cartItem.get());
    }

    @Override
    public void DeleteCartDetail(Long userId, String elite, Long bookId) {
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty()){
            throw new UserNotFoundException("User is not present in DB");
        }
        Boolean eliteUser;
        if(elite.equals("true")){
            eliteUser=true;
        }
        else {
            eliteUser=false;
        }
        Optional<CartItem> cartItem = cartItemRepository.findByBookIdAndCartId(bookId, user.get().getCart(), eliteUser);
        if(cartItem.isEmpty()){
            throw new ResourceNotFoundException("CartItem is not present for given id");
        }
        else{
            cartItemRepository.delete(cartItem.get());
        }

    }

    @Override
    public BookPurchaseResponse buyBookFromCart(String elite, Long bookId, String quantity,Long userId) {
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty()){
            throw new UserNotFoundException("User is not present in DB");
        }
        Boolean eliteUser;
        if(elite.equals("true")){
            eliteUser=true;
        }
        else {
            eliteUser=false;
        }
        Optional<CartItem> cartItem = cartItemRepository.findByBookIdAndCartId(bookId, user.get().getCart(), eliteUser);
        if(cartItem.isEmpty()){
            throw new ResourceNotFoundException("CartItem is not present for given id");
        }
        BookRentAndReturnRequest returnRequest = new BookRentAndReturnRequest();
        returnRequest.setQuantity(Integer.parseInt(quantity));
        if(elite.equals("true")) {
            returnRequest.setUserId(userId);
        }else{
            returnRequest.setUserId(-1l);
        }
        String apiUrl = adminBaseUrl.concat("/purchase-book/" +bookId+"/"+userId);
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(authUserName, authSecret);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<BookRentAndReturnRequest> entity = new HttpEntity<>(returnRequest, headers);
        ResponseEntity<BookPurchaseResponse> response = restTemplate.exchange(
                apiUrl,
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<BookPurchaseResponse>() {
                }
        );
        cartItemRepository.delete(cartItem.get());
        return response.getBody();
    }

    @Override
    public List<BookPurchaseResponse> buyAllBooks(Long userId) {
        try {
            Optional<User> user = userRepository.findById(userId);
            if (user.isEmpty()) {
                throw new UserNotFoundException("User is not present in DB");
            }
            List<CartItem> cartItemList = user.get().getCart().getCartItemList();
            if (cartItemList.isEmpty()) {
                throw new ResourceNotFoundException("Cart is empty");
            }

            List<BookRentAndReturnRequest> returnRequest = new ArrayList<>();
            for (CartItem cartItem : cartItemList) {
                BookRentAndReturnRequest request = new BookRentAndReturnRequest();
                request.setQuantity(cartItem.getQuantity());
                request.setBookId(cartItem.getBookId());
                if (cartItem.getIsElite().equals("true")) {
                    request.setUserId(userId);
                } else {
                    request.setUserId(-1l);
                }
                returnRequest.add(request);
            }
            String apiUrl = adminBaseUrl.concat("/buy-all-books/" + userId);
            HttpHeaders headers = new HttpHeaders();
            headers.setBasicAuth(authUserName, authSecret);
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<List<BookRentAndReturnRequest>> entity = new HttpEntity<>(returnRequest, headers);
            ResponseEntity<List<BookPurchaseResponse>> response = restTemplate.exchange(
                    apiUrl,
                    HttpMethod.POST,
                    entity,
                    new ParameterizedTypeReference<List<BookPurchaseResponse>>() {
                    }
            );
            Long cartId = user.get().getCart().getId();
            cartItemRepository.deleteByCartId(cartId);

            return response.getBody();
        }catch (Exception e){
            System.out.println(e.getLocalizedMessage());
        }
      return  null;
    }



}


