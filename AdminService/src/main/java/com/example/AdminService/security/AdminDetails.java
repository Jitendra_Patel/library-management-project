package com.example.AdminService.security;

import com.example.AdminService.entity.Admin;
import com.example.AdminService.exception.BookNotFoundException;
import com.example.AdminService.exception.ResourceNotFoundException;
import com.example.AdminService.repository.AdminRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AdminDetails  implements UserDetailsService {
    @Autowired
    private AdminRepository adminRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Admin> admin = adminRepository.findByEmail(username);
        if(admin.isEmpty()){
            try {
                throw new ResourceNotFoundException("Admin is not present");
            } catch (ResourceNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
          return new User(admin.get().getUsername(),admin.get().getPassword(),admin.get().getAuthorities());

    }
}
