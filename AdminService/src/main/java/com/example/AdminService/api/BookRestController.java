package com.example.AdminService.api;

import com.example.AdminService.dto.*;
import com.example.AdminService.entity.Book;
import com.example.AdminService.exception.BookNotFoundException;
import com.example.AdminService.exception.ResourceNotFoundException;
import com.example.AdminService.mapper.BookMapper;
import com.example.AdminService.service.BookTransactionService;
import com.example.AdminService.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class BookRestController {

    @Autowired
    private BookService bookService;
    @Autowired
    private BookTransactionService bookTransactionService;

    @GetMapping("/list-of-books")
    public ResponseEntity<?> getAllBooks(){
        try {
            List<BookResponse> books = bookService.getAllBooks();
            return ResponseEntity.ok(books);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/rent-book/{bookId}/{userId}")
    public ResponseEntity<?> rentBook(@PathVariable Long bookId, @PathVariable Long userId,
                                           @RequestBody @Valid BookRentAndReturnTransactionRequest
                                                   bookTransactionRequest){
        try {
            BookRentAndReturnTransactionResponse response = bookTransactionService.saveRentBookDetail(bookTransactionRequest, bookId, userId);
            return ResponseEntity.ok(response);
        } catch (ResourceNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }

    }


    @PostMapping("/return-book/{bookId}/{userId}")
    public ResponseEntity<?> returnBook(@PathVariable Long bookId,@PathVariable Long userId,
                                             @RequestBody BookRentAndReturnTransactionRequest bookTransactionRequest){
        try {
            BookRentAndReturnTransactionResponse returnResponse = bookTransactionService.
                    returnBookDetail(bookId, userId, bookTransactionRequest);
            return ResponseEntity.ok(returnResponse);
        } catch (ResourceNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/purchase-book/{bookId}/{userId}")
    public ResponseEntity<?> purchaseBook(@PathVariable Long bookId,@PathVariable Long userId,
                                               @RequestBody BookRentAndReturnTransactionRequest bookTransactionRequest){
        try {
            BookPurchaseResponse bookPurchaseResponse = bookTransactionService.purchaseBookDetail(bookId, userId, bookTransactionRequest);
            return ResponseEntity.ok(bookPurchaseResponse);
        } catch (ResourceNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/user-purchase-list/{userId}")
    public ResponseEntity<?> listOfPurchaseTransaction(@PathVariable Long userId){
        try {
            List<BookPurchaseResponse> bookPurchaseResponses =
                    bookTransactionService.getAllPurchaseTransactionOfUser(userId);
            return ResponseEntity.ok(bookPurchaseResponses);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/user-rent-return-list/{userId}")
    public ResponseEntity<?> listOfRentAndReturnTransaction(@PathVariable Long userId){
        try {
            List<BookRentAndReturnTransactionResponse> allRentAndReturnTransactionOfUser =
                    bookTransactionService.getAllRentAndReturnTransactionOfUser(userId);
            return ResponseEntity.ok(allRentAndReturnTransactionOfUser);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/trending-purchase-book")
    public ResponseEntity<?> highestPurchasedListOfBook(){
        try {
            List<TrendingBookResponse> purchases = bookTransactionService.countBookPurchasesByAllUsers();
            return ResponseEntity.ok(purchases);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/trending-rented-book")
    public ResponseEntity<?> highestRentedListOfBook(){
        try {
            List<TrendingBookResponse> countBookRentedByAllUsers = bookTransactionService.countBookRentedByAllUsers();
            return ResponseEntity.ok(countBookRentedByAllUsers);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/pagination")
    public ResponseEntity<?> paginationOnBooks(@RequestParam(defaultValue = "0",required = false) Integer page,
                                                        @RequestParam(defaultValue = "5",required = false)
                                                        Integer pageSize){
       try {
           Page<Book> books = bookService.paginationOnBook(page, pageSize);
           List<BookResponse> bookResponseOnPagination = books.stream()
                   .map(b -> {
                       BookResponse bookResponse = BookMapper.entityToDto(b);
                       bookResponse.setTotalPages(books.getTotalPages());
                       bookResponse.setTotalElements(books.getTotalElements());
                       return bookResponse;
                   })
                   .collect(Collectors.toList());

           return ResponseEntity.ok(bookResponseOnPagination);
       }catch (Exception e){
           return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
       }
    }

    @GetMapping("/get-book/{bookId}")
    public ResponseEntity<String> getBookById(@PathVariable Long bookId){
        try {
            String bookStatus = bookService.findBookByIdForUser(bookId);
            return ResponseEntity.ok(bookStatus);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/get-transaction/{userId}")
    public ResponseEntity<?> getAllTransactionOfUser(@PathVariable Long userId){
        try {
            List<BookTransactionResponse> allTransaction = bookTransactionService.getAllTransactionDoneByUser(userId);
            return ResponseEntity.ok(allTransaction);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("/buy-all-books/{userId}")
    public ResponseEntity<?>  purchaseAllBookOfCart(@PathVariable Long userId,
                                                                             @RequestBody List<BookRentAndReturnTransactionRequest>
                                                                                     bookRentAndReturnTransactionRequest) {
        try {
            List<BookPurchaseResponse> bookPurchaseResponses = bookTransactionService.buyAllBooksOfCart(userId, bookRentAndReturnTransactionRequest);
            return ResponseEntity.ok(bookPurchaseResponses);
        } catch (ResourceNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @GetMapping("/book-by-id/{bookId}")
    public ResponseEntity<?> getBook(@PathVariable Long bookId){
        try {
            BookResponse bookById = bookService.getBookById(bookId);
            return ResponseEntity.ok(bookById);
        } catch (BookNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
