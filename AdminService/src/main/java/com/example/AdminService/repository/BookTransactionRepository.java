package com.example.AdminService.repository;

import com.example.AdminService.entity.BookTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookTransactionRepository extends JpaRepository<BookTransaction,Long> {
    List<BookTransaction> findByUserId(Long userId);
}
