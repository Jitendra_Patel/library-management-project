package com.example.AdminService.repository;

import com.example.AdminService.entity.Admin;
import com.example.AdminService.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book,Long> {

    @Query("SELECT b FROM Book b WHERE b.admin = :admin AND b.isDeleted = false")
    List<Book> findByAdminAndNotDeleted(@Param("admin") Admin admin);

    @Query("SELECT b FROM Book b WHERE b.admin = :admin AND b.isDeleted = true")
    List<Book> findByAdminAndIsDeleted(@Param("admin") Admin admin);

    @Query("SELECT b FROM Book b WHERE b.isDeleted = false")
    Page<Book> findNotDeletedBooks(Pageable pageable);
}
