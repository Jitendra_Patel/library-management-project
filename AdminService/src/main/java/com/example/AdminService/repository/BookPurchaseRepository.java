package com.example.AdminService.repository;

import com.example.AdminService.entity.Book;
import com.example.AdminService.entity.BookPurchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookPurchaseRepository extends JpaRepository<BookPurchase,Long> {
    List<BookPurchase> findByBook(Book book);

    @Query("SELECT b FROM BookPurchase b WHERE  b.userId = :userId")
    List<BookPurchase> findBYUserId(Long userId);

    @Query("SELECT b.book.id, COUNT(b.book.id) FROM BookPurchase b GROUP BY b.book.id ORDER BY COUNT(b.book.id) DESC")
    List<Object[]> countBookPurchasesByBookId();
}
