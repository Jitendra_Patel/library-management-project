package com.example.AdminService.repository;

import com.example.AdminService.entity.Book;
import com.example.AdminService.entity.BookRentAndReturnTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookRentAndReturnTransactionRepository extends JpaRepository<BookRentAndReturnTransaction,Long> {

   @Query("SELECT b FROM BookRentAndReturnTransaction b WHERE b.book = :book AND b.userId = :userId AND b.isReturn = false" )
    Optional<BookRentAndReturnTransaction> findTransactionByUserIdAndBokId(@Param("book") Book book, @Param("userId") Long userId);

    List<BookRentAndReturnTransaction> findByBook(Book book);

    @Query("SELECT b FROM BookRentAndReturnTransaction b WHERE b.userId = :userId")
    List<BookRentAndReturnTransaction> findByUserId(@Param("userId") Long userId);

    @Query("SELECT b.book.id, COUNT(book.id) FROM BookRentAndReturnTransaction b GROUP BY b.book.id ORDER BY COUNT(book.id) DESC")
    List<Object[]> countBookRentedByBookId();
}
