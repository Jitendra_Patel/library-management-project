package com.example.AdminService.repository;

import com.example.AdminService.entity.BookQuantity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BookQuantityRepository extends JpaRepository<BookQuantity,Long> {
    Optional<BookQuantity> findByBookId(Long bookId);
}
