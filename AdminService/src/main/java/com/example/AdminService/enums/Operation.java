package com.example.AdminService.enums;

public enum Operation {

    RENT,
    RETURN,
    PURCHASE
}
