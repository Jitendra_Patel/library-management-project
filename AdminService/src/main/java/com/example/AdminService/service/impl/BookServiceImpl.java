package com.example.AdminService.service.impl;

import com.example.AdminService.dto.*;
import com.example.AdminService.entity.*;
import com.example.AdminService.exception.BookNotFoundException;
import com.example.AdminService.exception.ResourceNotFoundException;
import com.example.AdminService.mapper.BookMapper;

import com.example.AdminService.mapper.BookPurchaseMapper;
import com.example.AdminService.mapper.BookQuantityMapper;
import com.example.AdminService.mapper.BookRentAndReturnTransactionMapper;
import com.example.AdminService.repository.*;
import com.example.AdminService.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private BookQuantityRepository bookQuantityRepository;

    @Autowired
    private BookPurchaseRepository bookPurchaseRepository;

    @Autowired
    private BookRentAndReturnTransactionRepository bookRentAndReturnTransactionRepository;


    @Override
    public List<BookResponse> getAllBooks() {
        List<Book> books = bookRepository.findAll();
        return books.stream().map(BookMapper::entityToDto).collect(Collectors.toList());
    }

    @Override
    public void saveBook(BookRequest bookRequest, String username) {
        Optional<Admin> admin = adminRepository.findByEmail(username);
        Book book = BookMapper.dtoToEntity(bookRequest);
        book.setAdmin(admin.get());
        book.setIsDeleted(false);
        BookQuantity bookQuantity = new BookQuantity();
        bookQuantity.setBook(book);
        bookQuantity.setTotalQuantity(book.getTotalQuantity());
        bookQuantity.setRemainingQuantity(book.getTotalQuantity());
        bookRepository.save(book);
        bookQuantityRepository.save(bookQuantity);

    }

    @Override
    public void deleteBook(Long bookId) throws BookNotFoundException {
        Optional<Book> book = bookRepository.findById(bookId);
        System.out.println("LLLLL");
        if(book.isEmpty()){
            System.out.println("KKKK");
            throw new BookNotFoundException("Book not present for given id in DB...");
        }
        book.get().setIsDeleted(true);
        bookRepository.save(book.get());
    }

    @Override
    public BookResponse findBookDetailById(Long bookId) throws BookNotFoundException {
        Optional<Book> book = bookRepository.findById(bookId);
        if(book.isEmpty()){
            throw new BookNotFoundException("Book not present for given id in DB");
        }
        BookResponse bookResponse = BookMapper.entityToDto(book.get());
        return bookResponse;
    }

    @Override
    public void updateBook(BookRequest bookRequest, String username, Long bookId) {
        Optional<Admin> admin = adminRepository.findByEmail(username);
        Book book = BookMapper.dtoToEntity(bookRequest);
        book.setAdmin(admin.get());
        book.setIsDeleted(false);
        book.setId(bookId);
        bookRepository.save(book);
    }

    @Override
    public BookQuantityResponse getBookQuantity(Long bookId) throws BookNotFoundException {
        Optional<BookQuantity> bookQuantity = bookQuantityRepository.findByBookId(bookId);
        if(bookQuantity.isEmpty()){
            throw  new BookNotFoundException("Book Id not present in DB");
        }
        BookQuantityResponse bookQuantityResponse = BookQuantityMapper.entityToDto(bookQuantity.get());
        return bookQuantityResponse;
    }

    @Override
    public List<BookRentAndReturnTransactionResponse> getAllTransactionDetailsOfRentAndReturn(Long bookId) throws BookNotFoundException {
        Optional<Book> book = bookRepository.findById(bookId);
        if(book.isEmpty()){
            throw new BookNotFoundException("Book not present for given id in DB");
        }
        List<BookRentAndReturnTransaction> bookTransaction = bookRentAndReturnTransactionRepository.findByBook(book.get());
        List<BookRentAndReturnTransactionResponse> rentAndReturnResponse = bookTransaction.stream().map(bookRentAndReturnTransaction
                -> BookRentAndReturnTransactionMapper.entityToDto(bookRentAndReturnTransaction)).collect(Collectors.toList());
        return rentAndReturnResponse;
    }

    @Override
    public List<BookPurchaseResponse> getAllTransactionDetailOfPurchase(Long bookId) throws BookNotFoundException {
        Optional<Book> book = bookRepository.findById(bookId);
        if(book.isEmpty()){
            throw new BookNotFoundException("Book not present for given id in DB");
        }
        List<BookPurchase> bookTransaction = bookPurchaseRepository.findByBook(book.get());
        List<BookPurchaseResponse> purchaseResponse = bookTransaction.stream().map(bookPurchase ->
                BookPurchaseMapper.entityToDto(bookPurchase)).collect(Collectors.toList());
        return purchaseResponse;


    }

    @Override
    public List<BookResponse> getBooksAddedByAdminNotDeleted(String username) throws ResourceNotFoundException {
        Optional<Admin> admin = adminRepository.findByEmail(username);
        if(admin.isEmpty()){
            throw new ResourceNotFoundException("Admin is not present");
        }
        List<Book> adminBookList = bookRepository.findByAdminAndNotDeleted(admin.get());
        List<BookResponse> adminBookResponse = adminBookList.stream().map(bookResponse ->
                BookMapper.entityToDto(bookResponse)).collect(Collectors.toList());
        return adminBookResponse;
    }
    @Override
    public  Page<Book> paginationOnBook(Integer page,Integer pageSize){
        Pageable pageable = PageRequest.of(page, pageSize);
        Page<Book> bookPage =  bookRepository.findNotDeletedBooks(pageable);
        return bookPage ;
    }

    @Override
    public String findBookByIdForUser(Long bookId) {
        Optional<Book> book = bookRepository.findById(bookId);
        if(book.isEmpty()){
            return "Book is not present for given id in DB";
        }
        else {
            if (book.get().getIsDeleted()) {
                return "This Book has already deleted select other book";

            } else {
                return "Successfully";
            }
        }
    }

    @Override
    public List<BookResponse> getBooksAddedByAdminAreDeleted(String username) throws ResourceNotFoundException {
        Optional<Admin> admin = adminRepository.findByEmail(username);
        if(admin.isEmpty()){
            throw new ResourceNotFoundException("Admin is not present");
        }
        List<Book> adminBookList = bookRepository. findByAdminAndIsDeleted(admin.get());
        List<BookResponse> adminBookResponse = adminBookList.stream().map(bookResponse ->
                BookMapper.entityToDto(bookResponse)).collect(Collectors.toList());
        return adminBookResponse;
    }

    @Override
    public BookResponse getBookById(Long bookId) throws BookNotFoundException {
        Optional<Book> book = bookRepository.findById(bookId);
        if(book.isEmpty()){
            throw new BookNotFoundException("Book is not present in DB");
        }
        BookResponse bookResponse = BookMapper.entityToDto(book.get());
        return bookResponse;
    }


}
