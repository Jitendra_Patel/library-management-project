package com.example.AdminService.service.impl;

import com.example.AdminService.dto.*;
import com.example.AdminService.entity.*;
import com.example.AdminService.enums.Operation;
import com.example.AdminService.exception.ResourceNotFoundException;
import com.example.AdminService.mapper.BookPurchaseMapper;
import com.example.AdminService.mapper.BookRentAndReturnTransactionMapper;
import com.example.AdminService.mapper.BookTransactionMapper;
import com.example.AdminService.repository.*;
import com.example.AdminService.service.BookTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookTransactionServiceImpl implements BookTransactionService {
    @Autowired
    private BookRentAndReturnTransactionRepository bookRentAndReturnTransactionRepository;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private BookQuantityRepository bookQuantityRepository;
    @Autowired
    private BookPurchaseRepository bookPurchaseRepository;
    @Autowired
    private BookTransactionRepository bookTransactionRepository;




    @Override
    public BookRentAndReturnTransactionResponse saveRentBookDetail(BookRentAndReturnTransactionRequest bookTransactionRequest, Long bookId, Long userId) throws ResourceNotFoundException {

        Optional<Book> book = bookRepository.findById(bookId);
        if(book.isEmpty() ){
            throw  new ResourceNotFoundException("Book is not present for given ID in DB");
        }
        BookRentAndReturnTransactionResponse response = new BookRentAndReturnTransactionResponse();
        if(book.get().getIsDeleted()){
            response.setMessage(" This Book has already deleted select other book ");
            return response;
        }
        Optional<BookRentAndReturnTransaction> transactionDetail =
                bookRentAndReturnTransactionRepository.findTransactionByUserIdAndBokId(book.get(), userId);
        if(transactionDetail.isPresent()){
            response.setMessage("You have already rented this book");
            return response;
        }
        Integer rentDays= null;
        String expiryYear = bookTransactionRequest.getExpiryDate().toString().substring(0,4);
        String expiryMonth = bookTransactionRequest.getExpiryDate().toString().substring(5,7);
        String expiryDate = bookTransactionRequest.getExpiryDate().toString().substring(8);
        String purchaseYear = bookTransactionRequest.getPurchaseDate().toString().substring(0,4);
        String purchaseMonth = bookTransactionRequest.getPurchaseDate().toString().substring(5,7);
        String purchaseDate = bookTransactionRequest.getPurchaseDate().toString().substring(8);

       if(expiryYear.equals(purchaseYear) && expiryMonth.equals(purchaseMonth)) {
           rentDays = Integer.parseInt(expiryDate)-Integer.parseInt(purchaseDate);
       }
       else if(expiryYear.equals(purchaseYear)){
           Integer monthDifference =Integer.parseInt(expiryMonth)-Integer.parseInt(purchaseMonth);
           if(monthDifference>1) {
               rentDays = ((monthDifference - 1) * 31) + (31 - Integer.parseInt(expiryDate)) +
                       Integer.parseInt(purchaseDate);
           }
           else{
               rentDays =  (31 - Integer.parseInt(purchaseDate)) + Integer.parseInt(expiryDate);
           }
       }
       else{
           Integer yearDifference = Integer.parseInt(expiryYear)-Integer.parseInt(purchaseYear);
           Integer monthDifference = (12 - Math.abs(Integer.parseInt(expiryMonth) - Integer.parseInt(purchaseMonth)));
           if(yearDifference>1){
               rentDays = ((yearDifference-1)*365)+(monthDifference-1)*31+(31-Integer.parseInt(purchaseDate))+
                       Integer.parseInt(expiryDate);
           }
           else{
               rentDays = ((monthDifference-1)*31)+(31-Integer.parseInt(purchaseDate))+Integer.parseInt(expiryDate);
               System.out.println(rentDays);
           }
       }
        double rentWeek = Math.ceil((double) rentDays / 7);
        BookRentAndReturnTransaction bookRentAndReturnTransaction = BookRentAndReturnTransactionMapper.
                dtoToEntity(bookTransactionRequest);

        Optional<BookQuantity> bookQuantity = bookQuantityRepository.findByBookId(bookId);
        BookTransaction bookTransaction = new BookTransaction();
        bookTransaction.setQuantity(1);
        bookTransaction.setUserId(userId);
        bookTransaction.setOperation(Operation.RENT);
        bookTransaction.setOpenQuantity(bookQuantity.get().getRemainingQuantity());
        bookTransaction.setUpdatedQuantity(bookQuantity.get().getRemainingQuantity()-1);
        bookTransaction.setBook(book.get());

        if(bookTransactionRequest.getUserId() == -1l){
            bookTransaction.setTotalAmount((book.get().getPrice()*10)*rentWeek/100);
            bookRentAndReturnTransaction.setPaidBookRent((book.get().getPrice()*10)*rentWeek/100);
        }
        else{
            bookTransaction.setTotalAmount((book.get().getPrice()*8)*rentWeek/100);
            bookRentAndReturnTransaction.setPaidBookRent((book.get().getPrice()*8)*rentWeek/100);
        }
        bookRentAndReturnTransaction.setDiscountedBookRent((book.get().getPrice()*8)*rentWeek/100);
        bookRentAndReturnTransaction.setOriginalBookRent((book.get().getPrice()*10)*rentWeek/100);
        bookRentAndReturnTransaction.setTotalRent(bookRentAndReturnTransaction.getPaidBookRent());
        bookRentAndReturnTransaction.setBook(book.get());
        bookRentAndReturnTransaction.setBookPenalty(0.0);
        bookRentAndReturnTransaction.setUserId(userId);
        bookRentAndReturnTransaction.setIsReturn(false);


        bookQuantity.get().setRemainingQuantity(bookQuantity.get().getRemainingQuantity()-1);
        bookQuantity.get().setRentedQuantity(bookQuantity.get().getRentedQuantity()+1);
        BookRentAndReturnTransaction rentAndReturnTransaction = bookRentAndReturnTransactionRepository.save(bookRentAndReturnTransaction);
       bookTransactionRepository.save(bookTransaction);
        bookQuantityRepository.save(bookQuantity.get());


        BookRentAndReturnTransactionResponse rentResponse = BookRentAndReturnTransactionMapper.entityToDto(rentAndReturnTransaction);
        rentResponse.setMessage("transaction successfully done");
        rentResponse.setBookName(book.get().getTitle());
        return rentResponse;
    }


    @Override
    public BookRentAndReturnTransactionResponse returnBookDetail(Long bookId, Long userId, BookRentAndReturnTransactionRequest bookTransactionRequest) throws ResourceNotFoundException {
        Optional<Book> book = bookRepository.findById(bookId);
        if(book.isEmpty()){
            throw  new ResourceNotFoundException("Book is not present for given ID");
        }
        if(book.get().getIsDeleted()){
            BookRentAndReturnTransactionResponse response =  new BookRentAndReturnTransactionResponse();
            response.setMessage("This Book has already deleted select other book ");
            return response;
        }
        Optional<BookRentAndReturnTransaction> transactionDetail =
                bookRentAndReturnTransactionRepository.findTransactionByUserIdAndBokId(book.get(), userId);

        if(transactionDetail.isEmpty()){
            BookRentAndReturnTransactionResponse response =  new BookRentAndReturnTransactionResponse();
            response.setMessage("Rent the book before return it");
            return response;
        }
        else{
            transactionDetail.get().setReturnDate(LocalDate.parse(bookTransactionRequest.getReturnDate()));
            Integer returnDays = null;
            String expiryYear = transactionDetail.get().getExpectedReturnDate().toString().substring(0,4);
            String expiryMonth = transactionDetail.get().getExpectedReturnDate().toString().substring(5,7);
            String expiryDate = transactionDetail.get().getExpectedReturnDate().toString().substring(8);
            String returnYear = bookTransactionRequest.getReturnDate().toString().substring(0,4);
            String returnMonth = bookTransactionRequest.getReturnDate().toString().substring(5,7);
            String returnDate = bookTransactionRequest.getReturnDate().toString().substring(8);

            if(expiryYear.equals(returnYear) && expiryMonth.equals(returnMonth)){
                 returnDays = Integer.parseInt(returnDate) - Integer.parseInt(expiryDate);
            }
            else if(expiryYear.equals(returnYear)){
                Integer monthDifference =Integer.parseInt(returnMonth)-Integer.parseInt(expiryMonth);
                if(monthDifference>1) {
                    returnDays = ((monthDifference - 1) * 31) + (31 - Integer.parseInt(expiryDate)) +
                            Integer.parseInt(returnDate);
                }
                else{
                    returnDays =  (31 - Integer.parseInt(expiryDate)) + Integer.parseInt(returnDate);
                }
            }
            else{
                Integer yearDifference = Integer.parseInt(returnYear)-Integer.parseInt(expiryYear);
                Integer monthDifference = (12 - Math.abs(Integer.parseInt(expiryMonth) - Integer.parseInt(expiryMonth)));
                if(yearDifference>1){
                    returnDays = ((yearDifference-1)*365)+(monthDifference-1)*31+(31-Integer.parseInt(expiryMonth))+
                            Integer.parseInt(returnDate);
                }
                else{
                    returnDays = ((monthDifference-1)*31)+(31-Integer.parseInt(expiryMonth))+Integer.parseInt(returnDate);
                }
            }

            Optional<BookQuantity> bookQuantity = bookQuantityRepository.findByBookId(bookId);
            BookTransaction transaction = new BookTransaction();
            transaction.setBook(book.get());
            transaction.setUserId(userId);
            transaction.setQuantity(1);
            transaction.setOperation(Operation.RETURN);
            transaction.setOpenQuantity(bookQuantity.get().getRemainingQuantity());
            transaction.setUpdatedQuantity(bookQuantity.get().getRemainingQuantity()+1);
            transaction.setTotalAmount(0.0);

            if(returnDays > 0){
                double extraWeek = Math.ceil((double) returnDays / 7);
                if(bookTransactionRequest.getUserId() == -1l) {
                    transactionDetail.get().setBookPenalty((book.get().getPrice() * 20) * extraWeek / 100);
                    transactionDetail.get().setTotalRent(transactionDetail.get().getTotalRent() +
                            ((book.get().getPrice() * 20) * extraWeek / 100));
                    transaction.setTotalAmount((book.get().getPrice() * 20) * extraWeek / 100);
                }else{
                    transactionDetail.get().setBookPenalty((book.get().getPrice() * 16) * extraWeek / 100);
                    transactionDetail.get().setTotalRent(transactionDetail.get().getTotalRent() +
                            ((book.get().getPrice() * 16) * extraWeek / 100));
                    transaction.setTotalAmount((book.get().getPrice() * 16) * extraWeek / 100);
                }
            }


            bookQuantity.get().setRemainingQuantity(bookQuantity.get().getRemainingQuantity()+1);
            bookQuantity.get().setRentedQuantity(bookQuantity.get().getRentedQuantity()-1);
            transactionDetail.get().setIsReturn(true);
            BookRentAndReturnTransaction transactionResponse = bookRentAndReturnTransactionRepository.save(transactionDetail.get());
            bookTransactionRepository.save(transaction);
            bookQuantityRepository.save(bookQuantity.get());
            BookRentAndReturnTransactionResponse response = BookRentAndReturnTransactionMapper.entityToDto(transactionResponse);
            response.setMessage("transaction successfully done");
            response.setBookName(book.get().getTitle());
            return response;
        }

    }

    @Override
    public BookPurchaseResponse purchaseBookDetail(Long bookId, Long userId,
                                     BookRentAndReturnTransactionRequest bookTransactionRequest) throws ResourceNotFoundException {
        Optional<Book> book = bookRepository.findById(bookId);
        if(book.get().getBookQuantity().getRemainingQuantity() < bookTransactionRequest.getQuantity()){
            BookPurchaseResponse response = new BookPurchaseResponse();
            response.setMessage("This Book is not available in stock sorry for inconvenience");
            return response;
        }
        if (book.isEmpty()) {
            throw new ResourceNotFoundException("Book is not present for given ID");
        }
        if (book.get().getIsDeleted()) {
            BookPurchaseResponse response = new BookPurchaseResponse();
            response.setMessage("This Book has already deleted select other book");
            return response;
        }

        Optional<BookQuantity> bookQuantity = bookQuantityRepository.findByBookId(bookId);
        BookTransaction bookTransaction = new BookTransaction();
        bookTransaction.setQuantity(bookTransactionRequest.getQuantity());
        bookTransaction.setOperation(Operation.PURCHASE);
        bookTransaction.setUserId(userId);
        bookTransaction.setBook(book.get());
        bookTransaction.setOpenQuantity(bookQuantity.get().getRemainingQuantity());
        bookTransaction.setUpdatedQuantity(bookQuantity.get().getRemainingQuantity() - bookTransactionRequest.getQuantity());

        BookPurchase bookPurchase = new BookPurchase();
        if (bookTransactionRequest.getUserId() == -1l) {
            bookPurchase.setPricePerBook(book.get().getPrice());
            bookPurchase.setTotalAmount(book.get().getPrice()*bookTransactionRequest.getQuantity());
            bookTransaction.setTotalAmount(book.get().getPrice()*bookTransactionRequest.getQuantity());
        } else {
            bookPurchase.setPricePerBook((book.get().getPrice()) * 80 / 100);
            bookPurchase.setTotalAmount(((book.get().getPrice()) * 80 / 100)*bookTransactionRequest.getQuantity());
            bookTransaction.setTotalAmount(((book.get().getPrice()) * 80 / 100)*bookTransactionRequest.getQuantity());
        }
        bookPurchase.setDiscountedPrice((book.get().getPrice()) * 80 / 100);
        bookPurchase.setQuantity(bookTransactionRequest.getQuantity());
        bookPurchase.setBook(book.get());
        bookPurchase.setUserId(userId);
        bookPurchase.setBookTransaction(bookTransaction);
        bookPurchase.setOriginalPrice(book.get().getPrice());

        bookQuantity.get().setPurchaseQuantity(bookQuantity.get().getPurchaseQuantity() + bookTransactionRequest.getQuantity());
        bookQuantity.get().setRemainingQuantity(bookQuantity.get().getRemainingQuantity() - bookTransactionRequest.getQuantity());
        bookTransactionRepository.save(bookTransaction);
        BookPurchase purchase = bookPurchaseRepository.save(bookPurchase);
        bookQuantityRepository.save(bookQuantity.get());

        BookPurchaseResponse bookPurchaseResponse = BookPurchaseMapper.entityToDto(purchase);
        bookPurchaseResponse.setMessage("transaction successfully done");
        bookPurchaseResponse.setBookName(book.get().getTitle());
        return bookPurchaseResponse;


    }

    @Override
    public List<BookPurchaseResponse> getAllPurchaseTransactionOfUser(Long userId) {
        List<BookPurchase> bookPurchaseTransaction = bookPurchaseRepository.
                findBYUserId(userId);
//        List<BookPurchaseResponse> bookPurchasesResponses = bookPurchaseTransaction.stream().map(bookPurchase ->
//                BookPurchaseMapper.entityToDto(bookPurchase)).collect(Collectors.toList());

        List<BookPurchaseResponse> purchaseResponses = new ArrayList<>();
       for(BookPurchase bookPurchase : bookPurchaseTransaction){
           Optional<Book> book = bookRepository.findById(bookPurchase.getBook().getId());
           BookPurchaseResponse bookPurchaseResponse = BookPurchaseMapper.entityToDto(bookPurchase);
           bookPurchaseResponse.setBookName(book.get().getTitle());
           purchaseResponses.add(bookPurchaseResponse);
       }
        return  purchaseResponses;
    }

    @Override
    public List<BookRentAndReturnTransactionResponse> getAllRentAndReturnTransactionOfUser(Long userId) {
        List<BookRentAndReturnTransaction> returnTransaction = bookRentAndReturnTransactionRepository.findByUserId(userId);
//        List<BookRentAndReturnTransactionResponse> rentAndReturnTransactionResponses = returnTransaction.stream().
//                map(rentAndReturnTransaction -> BookRentAndReturnTransactionMapper.entityToDto
//                (rentAndReturnTransaction)).collect(Collectors.toList());

        List<BookRentAndReturnTransactionResponse> responseList = new ArrayList<>();
        for(BookRentAndReturnTransaction transaction : returnTransaction){
            Optional<Book> book = bookRepository.findById(transaction.getBook().getId());
            BookRentAndReturnTransactionResponse response = BookRentAndReturnTransactionMapper.
                    entityToDto(transaction);
            response.setBookName(book.get().getTitle());
            responseList.add(response);
        }

        return responseList;
    }

    @Override
    public List<TrendingBookResponse> countBookPurchasesByAllUsers() {
        List<Object[]> totalBookPurchased = bookPurchaseRepository.countBookPurchasesByBookId();
        List<TrendingBookResponse> resultList = totalBookPurchased.stream().map(result -> TrendingBookResponse.
                builder().bookId(Long.valueOf(result[0].toString())).
                totalCount(Long.valueOf(result[1].toString())).build()).collect(Collectors.toList());
        return resultList;
    }

    @Override
    public List<TrendingBookResponse> countBookRentedByAllUsers() {
        List<Object[]> totalBookRented = bookRentAndReturnTransactionRepository.countBookRentedByBookId();
        List<TrendingBookResponse> resultList = totalBookRented.stream().map(result -> TrendingBookResponse.
                builder().bookId(Long.valueOf(result[0].toString())).
                totalCount(Long.valueOf(result[1].toString())).build()).collect(Collectors.toList());

        return resultList;
    }

    @Override
    public List<BookTransactionResponse> getAllTransactionDoneByUser(Long userId) {
        List<BookTransaction> responses = bookTransactionRepository.findByUserId(userId);
        List<BookTransactionResponse> bookTransactionResponseList = responses.stream().map(bookTransaction -> BookTransactionMapper.entityToDto(bookTransaction)).
                collect(Collectors.toList());

        return bookTransactionResponseList;

    }

    @Override
    public List<BookPurchaseResponse> buyAllBooksOfCart(Long userId, List<BookRentAndReturnTransactionRequest> bookRentAndReturnTransactionRequest) throws ResourceNotFoundException {
        List<BookPurchaseResponse> responseList = new ArrayList<>();
        for(BookRentAndReturnTransactionRequest request : bookRentAndReturnTransactionRequest ) {
            System.out.println(request.getBookId());
            BookPurchaseResponse bookPurchaseResponse = purchaseBookDetail(request.getBookId(), userId, request);
            responseList.add(bookPurchaseResponse);
        }
        return responseList;
    }


}
