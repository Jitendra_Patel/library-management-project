package com.example.AdminService.service;

import com.example.AdminService.dto.*;
import com.example.AdminService.exception.ResourceNotFoundException;
import com.example.AdminService.mapper.BookRentAndReturnTransactionMapper;

import java.util.List;

public interface BookTransactionService {


    BookRentAndReturnTransactionResponse saveRentBookDetail(BookRentAndReturnTransactionRequest bookRentAndReturnTransaction,
                                                          Long bookId, Long userId) throws ResourceNotFoundException;

    BookRentAndReturnTransactionResponse returnBookDetail(Long bookId, Long userId, BookRentAndReturnTransactionRequest bookTransactionRequest) throws ResourceNotFoundException;

    BookPurchaseResponse purchaseBookDetail(Long bookId, Long userId, BookRentAndReturnTransactionRequest bookTransactionRequest) throws ResourceNotFoundException;

    List<BookPurchaseResponse> getAllPurchaseTransactionOfUser(Long userId);

    List<BookRentAndReturnTransactionResponse> getAllRentAndReturnTransactionOfUser(Long userId);

    List<TrendingBookResponse> countBookPurchasesByAllUsers();

    List<TrendingBookResponse> countBookRentedByAllUsers();


    List<BookTransactionResponse> getAllTransactionDoneByUser(Long userId);

    List<BookPurchaseResponse> buyAllBooksOfCart(Long userId, List<BookRentAndReturnTransactionRequest> bookRentAndReturnTransactionRequest) throws ResourceNotFoundException;
}