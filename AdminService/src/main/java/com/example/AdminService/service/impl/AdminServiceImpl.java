package com.example.AdminService.service.impl;

import com.example.AdminService.dto.AdminRequest;
import com.example.AdminService.entity.Admin;
import com.example.AdminService.enums.Role;
import com.example.AdminService.exception.ResourceNotFoundException;
import com.example.AdminService.mapper.AdminMapper;
import com.example.AdminService.repository.AdminRepository;
import com.example.AdminService.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public void saveAdmin(AdminRequest adminRequest) {
        Admin admin = AdminMapper.dtoToEntity(adminRequest);
        admin.setRole(Role.ROLE_ADMIN);
        admin.setPassword(passwordEncoder.encode(admin.getPassword()));
        adminRepository.save(admin);

    }

    @Override
    public AdminRequest adminFindById(Long adminId) throws ResourceNotFoundException {
        Optional<Admin> admin = adminRepository.findById(adminId);
        if(admin.isPresent()) {
          return   AdminMapper.entityToDto(admin.get());
        }
        else{
            throw new ResourceNotFoundException("Admin is not present for given Id");
        }

    }
}
