package com.example.AdminService.service;

import com.example.AdminService.dto.*;
import com.example.AdminService.entity.Book;
import com.example.AdminService.exception.BookNotFoundException;
import com.example.AdminService.exception.ResourceNotFoundException;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface BookService {


    List<BookResponse> getAllBooks();

    void saveBook(BookRequest bookRequest, String username);

    void deleteBook(Long bookId) throws BookNotFoundException;

    BookResponse findBookDetailById(Long bookId) throws BookNotFoundException;

    void updateBook(BookRequest bookRequest, String username, Long bookId);

    BookQuantityResponse getBookQuantity(Long bookId) throws BookNotFoundException;

    List<BookRentAndReturnTransactionResponse> getAllTransactionDetailsOfRentAndReturn(Long bookId) throws BookNotFoundException;

    List<BookPurchaseResponse> getAllTransactionDetailOfPurchase(Long bookId) throws BookNotFoundException;

    List<BookResponse> getBooksAddedByAdminNotDeleted(String username) throws ResourceNotFoundException;

    Page<Book> paginationOnBook(Integer page, Integer pageSize);

    String findBookByIdForUser(Long bookId);

    List<BookResponse> getBooksAddedByAdminAreDeleted(String username) throws ResourceNotFoundException;

    BookResponse getBookById(Long bookId) throws BookNotFoundException;
}
