package com.example.AdminService.service;

import com.example.AdminService.dto.AdminRequest;
import com.example.AdminService.entity.Admin;
import com.example.AdminService.exception.ResourceNotFoundException;

import java.util.Optional;

public interface AdminService {
    void saveAdmin(AdminRequest adminRequest);

    AdminRequest adminFindById(Long adminId) throws ResourceNotFoundException;
}
