package com.example.AdminService.entity;

import com.example.AdminService.enums.Operation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "book_transaction_detail")
public class BookTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "open_quantity")
    private Integer openQuantity;

    @Column(name = "updated_quantity")
    private Integer updatedQuantity;

    @Column(name = "total_amount")
    private Double totalAmount;

    @CreationTimestamp
    @Column(name = "transaction_date")
    private LocalDate transactionDate;

    @Column(name = "operation")
    @Enumerated(EnumType.STRING)
    private Operation operation;

    @Column(name = "quantity")
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    @Column(name = "user_id")
    private Long userId;
}
