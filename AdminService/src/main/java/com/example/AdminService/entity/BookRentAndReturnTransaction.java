package com.example.AdminService.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "rent_and_return_transaction")
public class BookRentAndReturnTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "purchase_date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate purchaseDate;

    @Column(name = "expected_return_date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate expectedReturnDate;

    @Column(name = "return_date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate returnDate = null;

    @Column(name = "original_rent")
    private Double originalBookRent;

    @Column(name = "discounted_rent")
    private Double discountedBookRent;

    @Column(name = "paid_rent")
    private Double paidBookRent;

    @Column(name = "book_penalty")
    private Double bookPenalty ;

    @Column(name = "total_rent")
    private Double totalRent;

    @Column(name = "is_return")
    private Boolean isReturn ;

    @Column(name = "user_id")
    private Long userId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id")
    private Book book;
}
