package com.example.AdminService.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "book_purchase")
public class BookPurchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "original_price")
    private Double originalPrice;

    @Column(name = "discounted_price")
    private Double discountedPrice;

    @Column(name = "book_quantity")
    private Integer quantity;

    @Column(name = "price_per_book")
    private Double pricePerBook;

    @Column(name = "total_amount")
    private Double totalAmount;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "purchase_date")
    @CreationTimestamp
    private LocalDate purchaseDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id")
    private Book book;

    @OneToOne
    @JoinColumn(name = "book_transaction_id")
    private BookTransaction bookTransaction;

}
