package com.example.AdminService.entity;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "book_quantity")
public class BookQuantity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "total_quantity")
    private Integer totalQuantity;

    @Column(name = "remaining_quantity")
    private Integer remainingQuantity;

    @Column(name = "rentedQuantity")
    private Integer rentedQuantity = 0;

    @Column(name = "purchase_quantity")
    private Integer purchaseQuantity = 0;

    @OneToOne
    @JoinColumn(name = "book_id")
    private Book book;

}