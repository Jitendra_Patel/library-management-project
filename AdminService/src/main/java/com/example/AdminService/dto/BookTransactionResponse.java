package com.example.AdminService.dto;

import com.example.AdminService.entity.Book;
import com.example.AdminService.enums.Operation;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookTransactionResponse {

    private Integer openQuantity;

    private Integer updatedQuantity;

    private Double totalAmount;

    private LocalDate transactionDate;

    private String operation;

    private Integer quantity;

    private Long bookId;

    private String bookName;

    private Long userId;
}
