package com.example.AdminService.dto;

import com.example.AdminService.entity.Book;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.time.LocalDate;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookRentAndReturnTransactionResponse {

    private LocalDate purchaseDate;

    private LocalDate expectedReturnDate;

    private LocalDate returnDate;

    private Double paidRent;

    private Double bookPenalty;

    private Double totalRent;

    private Long userId;

    private Long  bookId;

    private String bookName;

    private Double originalBookRent;

    private Double discountedBookRent;

    private String message;


}
