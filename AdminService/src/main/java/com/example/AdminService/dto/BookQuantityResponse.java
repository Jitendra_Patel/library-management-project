package com.example.AdminService.dto;

import lombok.*;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.Column;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookQuantityResponse {

    private Integer totalQuantity;

    private Integer remainingQuantity;

    private Integer rentedQuantity;

    private Integer purchaseQuantity;
}
