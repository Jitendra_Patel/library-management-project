package com.example.AdminService.dto;

import com.example.AdminService.entity.Admin;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookResponse {

    private Long id;

    private String title;

    private String author;

    private String genre;

    private Double price;

    private Integer publishedYear;

    private Integer totalQuantity;

    private String  adminEmail;

    private Integer totalPages;

    private Long totalElements;

    private Boolean isDeleted;

}
