package com.example.AdminService.dto;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookRentAndReturnTransactionRequest {

    private String purchaseDate;

    private String expiryDate;

    private String returnDate;

    private Long userId;

    private Integer quantity;

    private Long bookId;
}
