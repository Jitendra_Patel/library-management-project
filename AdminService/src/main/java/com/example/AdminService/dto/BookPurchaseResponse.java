package com.example.AdminService.dto;

import com.example.AdminService.entity.Book;
import com.example.AdminService.entity.BookTransaction;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Builder
public class BookPurchaseResponse {


    private Double originalPrice;

    private Double discountedPrice;

    private Integer quantity;

    private Double pricePerBook;

    private Double totalAmount;

    private Long userId;

    private LocalDate purchaseDate;

    private Long bookId;

    private String message;

    private String bookName;


}
