package com.example.AdminService.dto;

import com.example.AdminService.entity.Admin;
import com.example.AdminService.entity.BookQuantity;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookRequest {

    private Long id;

    @NotEmpty(message = "title should not empty")
    private String title;

    @NotEmpty(message = "author should not empty")
    private String author;

    @NotEmpty(message = "genre should not empty")
    private String genre;

    @NotNull(message = "price should not be null")
    @Min(value = 10,message = "price must be greater than 9")
    private Double price;

    @NotNull(message = "publishedYear should not empty")
    private Integer publishedYear;

    @NotNull(message = "quantity should not be null")
    @Min(value = 1,message = "Quantity can not be zero")
    private Integer totalQuantity;

}
