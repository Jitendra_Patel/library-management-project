package com.example.AdminService.mapper;

import com.example.AdminService.dto.BookQuantityResponse;
import com.example.AdminService.entity.BookQuantity;

public class BookQuantityMapper {

    public static BookQuantityResponse entityToDto(BookQuantity bookQuantity){

        return BookQuantityResponse.builder()
                .purchaseQuantity(bookQuantity.getPurchaseQuantity())
                .totalQuantity(bookQuantity.getTotalQuantity())
                .remainingQuantity(bookQuantity.getRemainingQuantity())
                .rentedQuantity(bookQuantity.getRentedQuantity())
                .build();
    }
}
