package com.example.AdminService.mapper;

import com.example.AdminService.dto.AdminRequest;
import com.example.AdminService.entity.Admin;

public class AdminMapper {

    public static Admin dtoToEntity(AdminRequest adminRequest){

        return Admin.builder()
                .email(adminRequest.getEmail())
                .name(adminRequest.getName())
                .password(adminRequest.getPassword())
                .build();

    }
    public static AdminRequest entityToDto(Admin admin){

        return AdminRequest.builder()
                .email(admin.getEmail())
                .name(admin.getUsername())
                .build();
    }

}
