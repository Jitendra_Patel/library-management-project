package com.example.AdminService.mapper;

import com.example.AdminService.dto.BookRequest;
import com.example.AdminService.dto.BookResponse;
import com.example.AdminService.entity.Book;

public class BookMapper {

    public static Book dtoToEntity(BookRequest bookRequest){
        return Book.builder()
                .genre(bookRequest.getGenre())
                .title(bookRequest.getTitle())
                .price(bookRequest.getPrice())
                .author(bookRequest.getAuthor())
                .publishedYear(bookRequest.getPublishedYear())
                .totalQuantity(bookRequest.getTotalQuantity())
                .build();
    }
    public static BookResponse entityToDto(Book book){
        return BookResponse.builder()
                .id(book.getId())
                .title(book.getTitle())
                .price(book.getPrice())
                .author(book.getAuthor())
                .publishedYear(book.getPublishedYear())
                .totalQuantity(book.getTotalQuantity())
                .genre(book.getGenre())
                .adminEmail(book.getAdmin().getEmail())
                .isDeleted(book.getIsDeleted())
                .build();

    }
}
