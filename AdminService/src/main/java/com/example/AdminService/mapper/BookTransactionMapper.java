package com.example.AdminService.mapper;

import com.example.AdminService.dto.BookTransactionResponse;
import com.example.AdminService.entity.BookTransaction;

public class BookTransactionMapper {

    public static BookTransactionResponse entityToDto(BookTransaction bookTransaction){

        return BookTransactionResponse.builder()
                .operation(bookTransaction.getOperation().toString())
                .transactionDate(bookTransaction.getTransactionDate())
                .openQuantity(bookTransaction.getOpenQuantity())
                .updatedQuantity(bookTransaction.getUpdatedQuantity())
                .bookId(bookTransaction.getBook().getId())
                .bookName(bookTransaction.getBook().getTitle())
                .userId(bookTransaction.getUserId())
                .quantity(bookTransaction.getQuantity())
                .totalAmount(bookTransaction.getTotalAmount())
                .build();
    }
}
