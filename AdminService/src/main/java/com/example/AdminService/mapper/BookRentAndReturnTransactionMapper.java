package com.example.AdminService.mapper;

import com.example.AdminService.dto.BookRentAndReturnTransactionRequest;
import com.example.AdminService.dto.BookRentAndReturnTransactionResponse;
import com.example.AdminService.entity.BookRentAndReturnTransaction;

import java.time.LocalDate;

public class BookRentAndReturnTransactionMapper {


    public static BookRentAndReturnTransaction dtoToEntity(BookRentAndReturnTransactionRequest bookTransaction){

        return BookRentAndReturnTransaction.builder()
                .expectedReturnDate(LocalDate.parse(bookTransaction.getExpiryDate()))
                .purchaseDate(LocalDate.parse(bookTransaction.getPurchaseDate()))
                .build();
    }

    public static BookRentAndReturnTransactionResponse entityToDto(BookRentAndReturnTransaction bookRentAndReturnTransaction){

        return BookRentAndReturnTransactionResponse.builder()
                .bookId(bookRentAndReturnTransaction.getBook().getId())
                .paidRent(bookRentAndReturnTransaction.getPaidBookRent())
                .originalBookRent(bookRentAndReturnTransaction.getOriginalBookRent())
                .discountedBookRent(bookRentAndReturnTransaction.getDiscountedBookRent())
                .bookPenalty(bookRentAndReturnTransaction.getBookPenalty())
                .totalRent(bookRentAndReturnTransaction.getTotalRent())
                .returnDate(bookRentAndReturnTransaction.getReturnDate())
                .userId(bookRentAndReturnTransaction.getUserId())
                .expectedReturnDate(bookRentAndReturnTransaction.getExpectedReturnDate())
                .purchaseDate(bookRentAndReturnTransaction.getPurchaseDate())
                .build();
    }
}
