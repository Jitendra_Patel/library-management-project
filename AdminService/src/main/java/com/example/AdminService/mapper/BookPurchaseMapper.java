package com.example.AdminService.mapper;

import com.example.AdminService.dto.BookPurchaseResponse;
import com.example.AdminService.entity.BookPurchase;

public class BookPurchaseMapper {

    public static BookPurchaseResponse entityToDto(BookPurchase bookPurchase){

        return BookPurchaseResponse.builder()
                .bookId(bookPurchase.getBook().getId())
                .quantity(bookPurchase.getQuantity())
                .pricePerBook(bookPurchase.getPricePerBook())
                .totalAmount(bookPurchase.getTotalAmount())
                .originalPrice(bookPurchase.getOriginalPrice())
                .discountedPrice(bookPurchase.getDiscountedPrice())
                .purchaseDate(bookPurchase.getPurchaseDate())
                .userId(bookPurchase.getUserId())
                .build();
    }
}
