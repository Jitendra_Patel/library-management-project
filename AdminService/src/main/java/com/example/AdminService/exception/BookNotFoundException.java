package com.example.AdminService.exception;

public class BookNotFoundException extends Exception{

    private String message;
    public BookNotFoundException(String message) {
        super((message));
        this.message=message;
    }
}
