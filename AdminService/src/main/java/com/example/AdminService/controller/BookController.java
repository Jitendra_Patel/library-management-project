package com.example.AdminService.controller;

import com.example.AdminService.dto.*;
import com.example.AdminService.entity.Book;
import com.example.AdminService.exception.BookNotFoundException;
import com.example.AdminService.exception.ResourceNotFoundException;
import com.example.AdminService.mapper.BookMapper;
import com.example.AdminService.service.BookTransactionService;
import com.example.AdminService.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class BookController {
    @Autowired
    private BookService bookService;

    @Autowired
    private BookTransactionService bookTransactionService;


    @GetMapping("/create-book")
    public String createBook(Model model){
        try {
            model.addAttribute("book", new BookRequest());
            return "createBook";
        }catch (Exception e){
            model.addAttribute("errorMessage",e.getMessage());
            return "errorPage";
        }
    }

    @PostMapping("/save-book")
    public String saveBook(@Valid @ModelAttribute("book") BookRequest bookRequest,
                       BindingResult result ,Model model) {
        try {
            if (result.hasErrors()) {
                model.addAttribute("book", bookRequest);
                return "createBook";
            }
            UserDetails loginUser = getLoginUser();
            bookService.saveBook(bookRequest, loginUser.getUsername());
            return "redirect:/books";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/delete-book/{bookId}")
    public String deleteBook(@PathVariable Long bookId,Model model){
        try {
            BookResponse bookResponse = bookService.findBookDetailById(bookId);
            if(bookResponse.getIsDeleted()){
                throw new BookNotFoundException("This book is already deleted");
            }
            UserDetails loginUser = getLoginUser();
            if ( !bookResponse.getAdminEmail().equals(loginUser.getUsername())){
                throw new BookNotFoundException("You don't have permission to delete this book");
            }
            bookService.deleteBook(bookId);
            return "redirect:/books";

        } catch (BookNotFoundException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/update-book/{bookId}")
    public String updateBook(@PathVariable Long bookId,Model model) {

        try {
            BookResponse bookResponse = bookService.findBookDetailById(bookId);

            if(bookResponse.getIsDeleted()){
                throw  new BookNotFoundException("This book is already deleted");
            }
            UserDetails loginUser = getLoginUser();
            if ( !bookResponse.getAdminEmail().equals(loginUser.getUsername())){
                throw new BookNotFoundException("You don't have permission to update this book");

            }
            model.addAttribute("book", bookResponse);
            return "updateBook";

        } catch (BookNotFoundException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "errorPage";
        }
    }


    @PostMapping("/change-book")
    public  String saveChangesOfBook(@Valid @ModelAttribute("book") BookRequest bookRequest,
                                    BindingResult result, @RequestParam Long bookId,Model model){
        try {
            if (result.hasErrors()) {
                bookRequest.setId(bookId);
                model.addAttribute("book", bookRequest);
                return "updateBook";
            }
            UserDetails loginUser = getLoginUser();
            bookService.updateBook(bookRequest, loginUser.getUsername(), bookId);
            return "redirect:/books";
        }catch (Exception e){
            model.addAttribute("errorMessage", e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/book-quantity/{bookId}")
    public String getBookQuantity(@PathVariable Long bookId,Model model)  {

        try {
            UserDetails loginUser = getLoginUser();
            BookResponse bookResponse = bookService.findBookDetailById(bookId);
            if ( !bookResponse.getAdminEmail().equals(loginUser.getUsername())) {
                throw new BookNotFoundException("You don't have permission to check the quantity of  this book");
            }
            BookQuantityResponse  bookQuantity = bookService.getBookQuantity(bookId);
            model.addAttribute("bookQuantity",bookQuantity);
            return "bookQuantity";

        } catch (BookNotFoundException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "errorPage";
        }

    }

    @GetMapping("/purchase-list/{bookId}")
    public String getAllPurchaseTransactionOfBook(@PathVariable Long bookId,Model model){

        try {
            BookResponse bookResponse = bookService.findBookDetailById(bookId);
            UserDetails loginUser = getLoginUser();
            if ( !bookResponse.getAdminEmail().equals(loginUser.getUsername())) {
                    throw new BookNotFoundException("You don't have permission to check the transaction of  this book");
                }
            List<BookPurchaseResponse> allTransactionDetailOfPurchase = bookService.getAllTransactionDetailOfPurchase(bookId);
            model.addAttribute("purchase",allTransactionDetailOfPurchase);
            return "purchaseTransactions";
        } catch (BookNotFoundException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "errorPage";
        }

        }

    @GetMapping("/rent-return-list/{bookId}")
    public String getAllRentAndReturnTransactionOfBook(@PathVariable Long bookId,Model model){

        try {
            BookResponse bookResponse = bookService.findBookDetailById(bookId);
            UserDetails loginUser = getLoginUser();
            if ( !bookResponse.getAdminEmail().equals(loginUser.getUsername())) {
                    throw new BookNotFoundException("You don't have permission to check the transaction of  this book");
                }
            List<BookRentAndReturnTransactionResponse> allTransactionDetailsOfRentAndReturn =
                    allTransactionDetailsOfRentAndReturn = bookService.getAllTransactionDetailsOfRentAndReturn(bookId);
            model.addAttribute("rentAndReturn",allTransactionDetailsOfRentAndReturn);
            return "rentAndReturnTransaction";
        } catch (BookNotFoundException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "errorPage";
        }

    }


    @GetMapping("/trending-purchase-books")
    public String listOfHighestPurchasedBook(Model model){
        try {
            List<TrendingBookResponse> purchases = bookTransactionService.countBookPurchasesByAllUsers();
            model.addAttribute("objects", purchases);
            return "trendingPage";
        }catch (Exception e){
            model.addAttribute("errorMessage", e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/trending-rented-books")
    public String listOfHighestRentedBook(Model model){
        try {
            List<TrendingBookResponse> countBookRentedByAllUsers = bookTransactionService.countBookRentedByAllUsers();
            model.addAttribute("object", countBookRentedByAllUsers);
            return "trendingPage";
        }catch (Exception e){
            model.addAttribute("errorMessage", e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/books")
    public String getAllBooksAddedByAdmin(Model model){

        try {
            UserDetails loginUser = getLoginUser();
            List<BookResponse> bookResponseList = bookService.getBooksAddedByAdminNotDeleted(loginUser.getUsername());
            model.addAttribute("bookResponse",bookResponseList);
            return  "profilePage";
        } catch (ResourceNotFoundException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "errorPage";
        }

    }

    @GetMapping("/deleted-book")
    public String getAllDeletedBookOfAdmin(Model model){

        try {
            UserDetails loginUser = getLoginUser();
            List<BookResponse> booksAddedByAdminAreDeleted = bookService.getBooksAddedByAdminAreDeleted(loginUser.getUsername());
            model.addAttribute("deletedBook",booksAddedByAdminAreDeleted);
            return "deletedBook";
        } catch (ResourceNotFoundException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "errorPage";
        }

    }


    @GetMapping("/home")
    public String paginationWithSorting(Model model,
                                        @RequestParam(defaultValue = "0",required = false) Integer page,
                                        @RequestParam(defaultValue = "5",required = false) Integer pageSize) {
        try {
            Page<Book> books = bookService.paginationOnBook(page, pageSize);
            List<Book> booksResponse = books.getContent();
            List<BookResponse> bookResponse = booksResponse.stream().map(b -> BookMapper.entityToDto(b)).collect(Collectors.toList());
            model.addAttribute("books", booksResponse);
            model.addAttribute("currentPage", page);
            model.addAttribute("totalPages", books.getTotalPages());
            model.addAttribute("totalItems", books.getTotalElements());
            return "homePage";
        }catch (Exception e){
            model.addAttribute("errorMessage", e.getMessage());
            return "errorPage";
        }
    }


    public UserDetails getLoginUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        return userDetails;
    }

}
