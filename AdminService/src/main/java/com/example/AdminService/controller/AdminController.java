package com.example.AdminService.controller;

import com.example.AdminService.dto.AdminRequest;
import com.example.AdminService.entity.Admin;
import com.example.AdminService.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class AdminController {

    @Autowired
    private AdminService adminService;


    @GetMapping("/login")
    public String login(){
        return "loginPage";
    }

    @GetMapping("/sign-up")
    public String signupPageAdmin(Model model){
        model.addAttribute("admin",new AdminRequest());
        return "signupPage";
    }

    @PostMapping("/save-user")
    public String saveAdmin(@Valid @ModelAttribute("admin")AdminRequest adminRequest,
                            BindingResult result, Model model){
       try {
           if (result.hasErrors()) {
               model.addAttribute("admin", adminRequest);
               return "signupPage";
           }
           adminService.saveAdmin(adminRequest);
           return "redirect:/login";
       }catch (Exception e){
           model.addAttribute("errorMessage",e.getMessage());
           return "errorPage";
       }
    }



}
